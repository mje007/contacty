package com.spacemonkey.contacty;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.Layout;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {
    public final String TABLE_NAME;
    public static final String ID = "id";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String PHONE_NUMBER = "phone_number";

    public DatabaseHelper(Context context,String name) {
        super(context, name, null, 1);
        TABLE_NAME=name;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableExpression = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                FIRST_NAME + " TEXT NOT NULL," +
                LAST_NAME + " TEXT NOT NULL," +
                PHONE_NUMBER + " TEXT NOT NULL) ";

        db.execSQL(createTableExpression);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addItem (Contact newContact){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(FIRST_NAME,newContact.getFirstName());
        cv.put(LAST_NAME,newContact.getLastName());
        cv.put(PHONE_NUMBER,newContact.getPhoneNumber());

        boolean success = (db.insert(TABLE_NAME, null, cv) != -1) ? true:false;

        db.close();
        return  success;
    }

    public ArrayList<Contact> getEveryItem (){
        ArrayList<Contact> queryResult = new ArrayList<>()  ;
        String query = "SELECT * FROM "+TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                int id = cursor.getInt(0);
                String firstName = cursor.getString(1);
                String lastName = cursor.getString(2);
                String phoneNumber = cursor.getString(3);

                Contact temp = new Contact(firstName,lastName,phoneNumber);
                temp.setId(id);
                queryResult.add(temp);
            }while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return  queryResult;
    }

    public Contact getItem(int id){
        Contact contact;
        SQLiteDatabase db = getWritableDatabase();
        String deleteQuery = "SELECT * FROM "+TABLE_NAME +" WHERE "+ID+ " = "+id;

        Cursor cursor = db.rawQuery(deleteQuery,null);

        if (cursor.moveToFirst()){
            int id_ = cursor.getInt(0);
            String firstName = cursor.getString(1);
            String lastName = cursor.getString(2);
            String phoneNumber = cursor.getString(3);

            contact = new Contact(firstName,lastName,phoneNumber);
            contact.setId(id_);

        }else {
            contact = new Contact();
            contact.setId(-1);
        }

        cursor.close();
        db.close();
        return contact;
    }

    public boolean deleteItem (int id){
        SQLiteDatabase db = getWritableDatabase();
        boolean success = (db.delete(TABLE_NAME,ID+" = "+id,null)>0) ? true:false;
        db.close();
        return success;
    }

    public boolean updateItem (int id ,Contact newContact) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(FIRST_NAME,newContact.getFirstName());
        cv.put(LAST_NAME,newContact.getLastName());
        cv.put(PHONE_NUMBER,newContact.getPhoneNumber());

        boolean success = (db.update(TABLE_NAME,cv,ID+" = "+id,null)>0) ? true : false;

        db.close();
        return success;
    }

    public int exist (Contact contact){
        SQLiteDatabase db = getWritableDatabase();
        String existenceQuery = "SELECT * FROM "+TABLE_NAME +" WHERE "+FIRST_NAME+ " = \'"+contact.getFirstName()
                + "\' COLLATE NOCASE AND "+ LAST_NAME +" = \'" +contact.getLastName() +"\' COLLATE NOCASE ";

        Cursor cursor = db.rawQuery(existenceQuery,null);


        int exists = (cursor.moveToFirst()) ? cursor.getInt(0) : -1;

        cursor.close();
        db.close();

        return exists;
    }
}

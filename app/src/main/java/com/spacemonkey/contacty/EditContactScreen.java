package com.spacemonkey.contacty;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EditContactScreen extends AppCompatActivity {

    EditText firstName;
    EditText lastName;
    EditText phoneNumber;
    Button update;
    Button returnBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact_screen);

        init();

        //Getting id of the contact that going to be shown on screen
        String contactID = getIntent().getStringExtra("id");

        //creating a database helper and getting contact from database
        DatabaseHelper databaseHelper = new DatabaseHelper(EditContactScreen.this,"Contacts");
        Contact contact = databaseHelper.getItem(Integer.parseInt(contactID));

        //setting views text property
        firstName.setText(contact.getFirstName());
        lastName.setText(contact.getLastName());
        phoneNumber.setText(contact.getPhoneNumber());


        //Listeners
        returnBtn.setOnClickListener(new ReturnToMain(EditContactScreen.this));

        update.setOnClickListener(v -> {

            //checks if fields are filled
            if (!inputFieldCheck()){
                Toast.makeText(EditContactScreen.this,getString(R.string.empty_field),Toast.LENGTH_LONG).show();
                return;
            }

            //Positive option action of dialog box
            EasyDialogBox.Operation yesOption = ()->{
                //creating the new and updated contact
                Contact newContact =  new Contact(firstName.getText().toString(),
                         lastName.getText().toString(),phoneNumber.getText().toString());

                //updating the database
                if (databaseHelper.updateItem(Integer.parseInt(contactID),newContact)) {
                    Toast.makeText(EditContactScreen.this,getString(R.string.updated),Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(EditContactScreen.this,getString(R.string.database_error),Toast.LENGTH_LONG).show();
                }

            } ;

            //Negative option action of dialog box
            EasyDialogBox.Operation noOption = ()->{
                Toast.makeText(EditContactScreen.this,getString(R.string.canceled),Toast.LENGTH_SHORT).show();
            } ;


            //Showing the dialog box
            new EasyDialogBox(EditContactScreen.this, getString(R.string.conformation),
                    getString(R.string.update_conformation_message),getString(R.string.yes_button),getString(R.string.no_button),yesOption,noOption)
                    .show();

        });

    }

    protected void init(){
        firstName = findViewById(R.id.edit_screen_firstName);
        lastName = findViewById(R.id.edit_screen_LastName);
        phoneNumber = findViewById(R.id.edit_screen_phone);
        returnBtn = findViewById(R.id.edit_screen_return_button);
        update = findViewById(R.id.edit_screen_edit_button);
    }

    boolean inputFieldCheck(){
        boolean isOk = true;

        if (firstName.getText().toString().isEmpty()){
            firstName.setError(getString(R.string.is_empty));
            isOk = false;
        }
        if (lastName.getText().toString().isEmpty()){
            lastName.setError(getString(R.string.is_empty));
            isOk = false;
        }
        if (phoneNumber.getText().toString().isEmpty()){
            phoneNumber.setError(getString(R.string.is_empty));
            isOk = false;
        }

        return isOk;
    }

    @Override
    public void onBackPressed() {
        new ReturnToMain(EditContactScreen.this);
    }
}
package com.spacemonkey.contacty;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ContactDetailScreen extends AppCompatActivity {

    TextView firstName;
    TextView lastName;
    TextView phoneNumber;

    Button call;
    Button sms;
    Button edit;
    Button delete;
    Button returnBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail_screen);


        init();


        //Getting id of the contact that going to be shown on screen
        String contactID = getIntent().getStringExtra("id");

        //creating a database helper and getting contact from database
        DatabaseHelper databaseHelper = new DatabaseHelper(ContactDetailScreen.this,"Contacts");
        Contact contact = databaseHelper.getItem(Integer.parseInt(contactID));

        //setting views text property
        firstName.setText(contact.getFirstName());
        lastName.setText(contact.getLastName());
        phoneNumber.setText(contact.getPhoneNumber());


        //listeners
        returnBtn.setOnClickListener(new ReturnToMain(ContactDetailScreen.this));

        edit.setOnClickListener(v -> {
            Intent intent = new Intent(ContactDetailScreen.this, EditContactScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("id",contactID);
            startActivity(intent);
        });

        delete.setOnClickListener(v -> {
            EasyDialogBox.Operation yesOption = ()->{
                databaseHelper.deleteItem(Integer.parseInt(contactID));
                firstName.setText("");
                lastName.setText("");
                phoneNumber.setText("");

                new ReturnToMain(ContactDetailScreen.this).run();
            };

            new EasyDialogBox(ContactDetailScreen.this,getString(R.string.conformation),
                    getString(R.string.delete_contact_conformation),getString(R.string.yes_button),getString(R.string.no_button),yesOption,null)
                    .show();
        });

        call.setOnClickListener(v -> {
            Intent intentDial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber.getText().toString()));
            startActivity(intentDial);
        });

        sms.setOnClickListener(v -> {
            String number = phoneNumber.getText().toString();  // The number on which you want to send SMS
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)));
        });

    }

    protected void init (){

        firstName = findViewById(R.id.contact_detail_firstName);
        lastName = findViewById(R.id.contact_detail_lastName);
        phoneNumber = findViewById(R.id.contact_detail_phoneNumber);
        call = findViewById(R.id.call);
        sms = findViewById(R.id.sms);
        edit = findViewById(R.id.detail_screen_edit_button);
        delete = findViewById(R.id.detail_screen_delete_button);
        returnBtn = findViewById(R.id.detail_screen_return_button);
    }

    @Override
    public void onBackPressed() {
        new ReturnToMain(ContactDetailScreen.this);
    }
}
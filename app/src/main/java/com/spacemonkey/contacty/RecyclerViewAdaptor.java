package com.spacemonkey.contacty;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdaptor extends RecyclerView.Adapter<RecyclerViewAdaptor.ViewHolder> {
    Context context;
    ArrayList<Contact> contacts;

    public RecyclerViewAdaptor(Context context ,ArrayList<Contact> contacts ){
        this.context=context;
        this.contacts = contacts;
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_contact,parent,false));
    }

    @Override
    public void onBindViewHolder( ViewHolder holder, int position) {
        Contact contact = contacts.get(position);
        holder.name.setText(contact.getFirstName().toString()+" "+contact.getLastName().toString());
        holder.phoneNumber.setText(contact.getPhoneNumber());

        View.OnClickListener click = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ContactDetailScreen.class);
                intent.putExtra("id",Integer.toString(contact.getId()));
                context.startActivity(intent);
            }
        };

        holder.phoneNumber.setOnClickListener(click);
        holder.name.setOnClickListener(click);
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,phoneNumber;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            context = itemView.getContext();
            name = itemView.findViewById(R.id.recyclerViewAdaptor_contact_name_textView);
            phoneNumber = itemView.findViewById(R.id.recyclerViewAdaptor_contact_phoneNumber_textView);

        }
    }
}

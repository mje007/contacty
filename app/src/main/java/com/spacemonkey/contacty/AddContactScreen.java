package com.spacemonkey.contacty;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.function.Function;

public class AddContactScreen extends AppCompatActivity {


    Button addContact;
    Button returnToMainScreen;
    EditText firstName;
    EditText lastName;
    EditText phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact_screen);

        init();

        //Listeners
        returnToMainScreen.setOnClickListener(new ReturnToMain(AddContactScreen.this));

        addContact.setOnClickListener(v -> {

            //Checks if all the fields are filled
            if (!inputFieldCheck()) {
                Toast.makeText(AddContactScreen.this,getString(R.string.empty_field),Toast.LENGTH_LONG).show();
                return;
            }

            //creating new contact
            Contact newContact;
            try {
                newContact = new Contact(firstName.getText().toString()
                        , lastName.getText().toString()
                        , phoneNumber.getText().toString());
            }catch (Exception e){
                phoneNumber.setError(getString(R.string.unacceptable));
                return;
            }

            //creating database helper
            DatabaseHelper dbHelper = new DatabaseHelper(AddContactScreen.this, "Contacts");

            //Checking if that the contact already exist
            if (dbHelper.exist(newContact)>-1){

                new EasyDialogBox(AddContactScreen.this,getString(R.string.contact_already_exist), getString(R.string.contact_already_exist_message),
                        getString(R.string.yes_button),getString(R.string.no_button),
                        () ->  {
                            Intent intent = new Intent(AddContactScreen.this, EditContactScreen.class);
                            intent.putExtra("id",String.valueOf(dbHelper.exist(newContact)));
                            startActivity(intent);
                        },
                        () -> {
                            //Nothing
                        }
                ).show();
                return;
            }

            //add contact, if adding was not successful show a dialog
            if (!dbHelper.addItem(newContact)) {
                new EasyDialogBox(AddContactScreen.this, getString(R.string.database_error), "unknown database error", getString(R.string.ok_button),
                        () -> {
                            //Clear EditTexts
                            firstName.setText("");
                            lastName.setText("");
                            phoneNumber.setText("");
                        }
                ).show();
                return;
            }

            //Showing success dialog
            new EasyDialogBox(AddContactScreen.this, getString(R.string.conformation), getString(R.string.contact_saved), getString(R.string.ok_button),
                    () -> {
                        //Clear EditTexts
                        firstName.setText("");
                        lastName.setText("");
                        phoneNumber.setText("");
                    }
            ).show();

        });

    }

    @Override
    public void onBackPressed() {
        new ReturnToMain(AddContactScreen.this);
    }

    protected void init(){
        //Init
        addContact = findViewById(R.id.add_contact_add_button);
        returnToMainScreen = findViewById(R.id.add_contact_return_button);
        firstName = findViewById(R.id.add_contact_firstName_textView);
        lastName = findViewById(R.id.add_contact_lastName_textView);
        phoneNumber = findViewById(R.id.add_contact_phoneNumber_textView);
    }

    boolean inputFieldCheck(){
        boolean isOk = true;

        if (firstName.getText().toString().isEmpty()){
            firstName.setError(getString(R.string.is_empty));
            isOk = false;
        }
        if (lastName.getText().toString().isEmpty()){
            lastName.setError(getString(R.string.is_empty));
            isOk = false;
        }
        if (phoneNumber.getText().toString().isEmpty()){
            phoneNumber.setError(getString(R.string.is_empty));
            isOk = false;
        }

        return isOk;
    }
}
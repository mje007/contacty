package com.spacemonkey.contacty;

import android.content.Intent;

import java.util.List;

public class Contact {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private int id;

    public Contact(String firstName, String lastName, String phoneNumbers ) {
       try {
            setFirstName(firstName);
            setLastName(lastName);
            setPhoneNumbers(phoneNumbers);
            this.id=-1;//A raw contact not created from db will have -1 as id
        } catch (IllegalArgumentException err){
            throw new IllegalArgumentException("Failed To Construct The Contact,{"+err.toString()+"}");
       }
    }

    public Contact() {
        firstName = null;
        lastName  = null;
        phoneNumber = null;
    }

    boolean isNull (){
        return (firstName==null||lastName==null||phoneNumber==null);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        if (firstName.isEmpty()||firstName==null){
            throw new IllegalArgumentException("Empty Argument,First Name");
        }
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        if (lastName.isEmpty()||lastName==null){
            throw new IllegalArgumentException("Empty Argument,Last Name");
        }
        this.lastName = lastName;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        if (phoneNumbers.isEmpty()||phoneNumbers==null){
            throw new IllegalArgumentException("Empty Argument,Phone Number");
        }else if (!isPhoneNumbersAcceptable(phoneNumbers)){
            throw new IllegalArgumentException("Unacceptable Argument,Phone Number");
        }
        this.phoneNumber = phoneNumbers;
    }

    public static boolean isPhoneNumbersAcceptable(String phoneNumbers){
        for (char character : phoneNumbers.toCharArray()) {
            if (!(Character.isDigit(character)||character=='+')) return false;
        }
        return true;
    }

    public String getFirstName() { return firstName; }

    public String getLastName() { return lastName; }

    public String getPhoneNumber() { return phoneNumber; }

    public int getId() { return id; }
}

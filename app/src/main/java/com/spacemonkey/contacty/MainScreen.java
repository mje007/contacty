package com.spacemonkey.contacty;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.function.Function;


public class MainScreen extends AppCompatActivity {
    FloatingActionButton addContact_Button;
    RecyclerView contacts_RecyclerView;
    ArrayList<Contact> allContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        //Init Views
        addContact_Button = findViewById(R.id.main_screen_add_button);
        contacts_RecyclerView = findViewById(R.id.main_screen_contact_recyclerView);

        //Set listeners
        addContact_Button.setOnClickListener(v -> {
            Intent intent = new Intent(MainScreen.this, AddContactScreen.class);
            intent.putExtra("databaseName","Contacts");
            startActivity(intent);
        });
        DatabaseHelper databaseHelper = new DatabaseHelper(MainScreen.this,"Contacts");

        allContacts = databaseHelper.getEveryItem();
        updateRecyclerView();

    }

    void updateRecyclerView(){
        RecyclerViewAdaptor adaptor = new RecyclerViewAdaptor(MainScreen.this,allContacts);
        contacts_RecyclerView.setAdapter(adaptor);
        contacts_RecyclerView.setLayoutManager(new LinearLayoutManager(MainScreen.this));
    }

}


class ReturnToMain implements View.OnClickListener {
    Context from;

    ReturnToMain(Context from){
        this.from = from;
    }

    void run(){
        onClick(null);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(from, MainScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        from.startActivity(intent);
    }
}
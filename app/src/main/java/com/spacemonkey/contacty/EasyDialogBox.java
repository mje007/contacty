package com.spacemonkey.contacty;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;


/*
This is basically a wrapper class for DialogBox with easy to use constructors
 */
public class EasyDialogBox extends AlertDialog {
    interface Operation {
        public void run() ;
    }

    EasyDialogBox(Context context , String title , String message ){
        super(context);
        super.setIcon(android.R.drawable.ic_dialog_info);
        super.setTitle(title);
        super.setMessage(message);
        super.setButton(BUTTON_NEUTRAL,(CharSequence) message,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Nothing
            }
        });
    }

    EasyDialogBox(Context context , String title , String message , String buttonText , Operation neutralOption ){
        super(context);
        super.setIcon(android.R.drawable.ic_dialog_info);
        super.setTitle(title);
        super.setMessage(message);
        super.setButton(BUTTON_NEUTRAL,(CharSequence)buttonText,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               neutralOption.run();
            }
        });
    }

    EasyDialogBox(Context context , String title , String message , String positiveBtnText , String negativeBtnText , Operation positiveOption , Operation negativeOption ){
        super(context);
        super.setIcon(android.R.drawable.ic_dialog_info);
        super.setTitle(title);
        super.setMessage(message);
        super.setButton(BUTTON_POSITIVE,positiveBtnText,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                positiveOption.run();
            }
        });
        super.setButton(BUTTON_NEGATIVE,negativeBtnText,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                negativeOption.run();
            }
        });
    }

}
